# BOTW PS5 Controller Mod

A breath of the wild ps5 controller UI mod

## ___THIS MOD IS STILL IN DEVELOPMENT!___
The only thing that _**100%**_ works is the controller font.
***

## Install Instructions 

> Copy content


## Current Progress 
### Replaced Controller Font
<details><summary>Controls</summary>
![Image](https://gitlab.com/Lepidopteran/botw-ps5-ui/-/raw/main/res/butt.svg "Button Layout")
</details>

### TODO
- [x] Replace font UI
- [ ] Replace Wii U gamepad with PS5 Controller

Made with pure love for botw :heart:
